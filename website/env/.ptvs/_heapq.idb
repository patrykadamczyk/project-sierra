�}q (X   docqX�  Heap queue algorithm (a.k.a. priority queue).

Heaps are arrays for which a[k] <= a[2*k+1] and a[k] <= a[2*k+2] for
all k, counting elements from 0.  For the sake of comparison,
non-existing elements are considered to be infinite.  The interesting
property of a heap is that a[0] is always its smallest element.

Usage:

heap = []            # creates an empty heap
heappush(heap, item) # pushes a new item on the heap
item = heappop(heap) # pops the smallest item from the heap
item = heap[0]       # smallest item on the heap without popping it
heapify(x)           # transforms list into a heap, in-place, in linear time
item = heapreplace(heap, item) # pops and returns smallest item, and adds
                               # new item; the heap size is unchanged

Our API differs from textbook heap algorithms as follows:

- We use 0-based indexing.  This makes the relationship between the
  index for a node and the indexes for its children slightly less
  obvious, but is more suitable since Python uses 0-based indexing.

- Our heappop() method returns the smallest item, not the largest.

These two make it possible to view the heap as a regular Python list
without surprises: heap[0] is the smallest item, and heap.sort()
maintains the heap invariant!
qX   membersq}q(X   heapifyq}q(X   kindqX   functionqX   valueq	}q
(hX;   Transform list into a heap, in-place, in O(len(heap)) time.qX	   overloadsq]q(}q(X   argsq}q(X   nameqhX
   arg_formatqX   *qu}q(hX   kwargsqhX   **qu�qhX;   Transform list into a heap, in-place, in O(len(heap)) time.qu}q(X   ret_typeq]qX   __builtin__qX   NoneTypeq�qaX   argsq}q (X   typeq!]q"hX   listq#�q$aX   nameq%X   listq&u�q'ueuuX
   __loader__q(}q)(hX   typerefq*h	]q+X   _frozen_importlibq,X   BuiltinImporterq-�q.auX   _heapreplace_maxq/}q0(hhh	}q1(hX   Maxheap variant of heapreplaceq2h]q3}q4(h}q5(hhhhu}q6(hhhhu�q7hX   Maxheap variant of heapreplaceq8uauuX	   __about__q9}q:(hX   dataq;h	}q<X   typeq=]q>(X   builtinsq?X   strq@�qAhX   strqB�qCesuX   __name__qD}qE(hh;h	}qFh=]qG(hAhCesuX   heappopqH}qI(hhh	}qJ(hXC   Pop the smallest item off the heap, maintaining the heap invariant.qKh]qL(}qM(h}qN(hhhhu}qO(hhhhu�qPhXC   Pop the smallest item off the heap, maintaining the heap invariant.qQu}qR(h]qShX   objectqT�qUah}qV(h!]qWh$ah%X   listqXu�qYueuuX   __doc__qZ}q[(hh;h	}q\h=]q](hAhCesuX   __spec__q^}q_(hh;h	}q`h=]qah,X
   ModuleSpecqb�qcasuX   heapreplaceqd}qe(hhh	}qf(hX�  heapreplace(heap, item) -> value. Pop and return the current smallest value, and add the new item.

This is more efficient than heappop() followed by heappush(), and can be
more appropriate when using a fixed-size heap.  Note that the value
returned may be larger than item!  That constrains reasonable uses of
this routine unless written as part of a conditional replacement:

    if item > heap[0]:
        item = heapreplace(heap, item)
qgh]qh(}qi(h}qjhX   heapqks}qlhX   itemqms�qnhX�  . Pop and return the current smallest value, and add the new item.

This is more efficient than heappop() followed by heappush(), and can be
more appropriate when using a fixed-size heap.  Note that the value
returned may be larger than item!  That constrains reasonable uses of
this routine unless written as part of a conditional replacement:

    if item > heap[0]:
        item = heapreplace(heap, item)
qoX   ret_typeqp]qqX    qrX   valueqs�qtau}qu(h]qvhUah}qw(h!]qxh$ah%X   listqyu}qz(h!]q{hUah%X   itemq|u�q}ueuuX   __package__q~}q(hh;h	}q�h=]q�(hAhesuX   _heappop_maxq�}q�(hhh	}q�(hX   Maxheap variant of heappop.q�h]q�}q�(h}q�(hhhhu}q�(hhhhu�q�hX   Maxheap variant of heappop.q�uauuX   heappushq�}q�(hhh	}q�(hXR   heappush(heap, item) -> None. Push item onto heap, maintaining the heap invariant.q�h]q�(}q�(h}q�hX   heapq�s}q�hX   itemq�s�q�hX6   . Push item onto heap, maintaining the heap invariant.q�hp]q�h?X   NoneTypeq��q�au}q�(h]q�hah}q�(h!]q�h$ah%X   listq�u}q�(h!]q�hUah%X   itemq�u�q�ueuuX   heappushpopq�}q�(hhh	}q�(hX�   heappushpop(heap, item) -> value. Push item on the heap, then pop and return the smallest item
from the heap. The combined action runs more efficiently than
heappush() followed by a separate call to heappop().q�h]q�(}q�(h}q�hX   heapq�s}q�hX   itemq�s�q�hX�   . Push item on the heap, then pop and return the smallest item
from the heap. The combined action runs more efficiently than
heappush() followed by a separate call to heappop().q�hp]q�htau}q�(h]q�hUah}q�(h!]q�h$ah%X   listq�u}q�(h!]q�hUah%X   itemq�u�q�ueuuX   _heapify_maxq�}q�(hhh	}q�(hX   Maxheap variant of heapify.q�h]q�}q�(h}q�(hhhhu}q�(hhhhu�q�hX   Maxheap variant of heapify.q�uauuh-}q�(hh=h	}q�(X   mroq�]q�(h.h?X   objectqȆq�eX   basesq�]q�h�ahX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    q�X	   is_hiddenq͈h}q�(X   module_reprq�}q�(hhh	}q�(hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        q�h]q�}q�(h}q�(hhhhu}q�(hhhhu�q�hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        q�uauuX
   __format__q�}q�(hX   methodq�h	}q�(hX   default object formatterq�h]q�}q�(h}q�(hhhhu}q�(hhhhu�q�hX   default object formatterq�uauuX   __lt__q�}q�(hh�h	}q�(hX   Return self<value.q�h]q�}q�(h}q�(hhhhu}q�(hhhhu�q�hX   Return self<value.q�uauuX   exec_moduleq�}q�(hhh	}q�(hX   Exec a built-in moduleq�h]q�}q�(h}q�(hhhhu}q�(hhhhu�q�hX   Exec a built-in moduleq�uauuX   __str__q�}q�(hh�h	}q�(hX   Return str(self).q�h]q�}q�(h}q�(hhhhu}q�(hhhhu�r   hX   Return str(self).r  uauuX   find_moduler  }r  (hhh	}r  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  h]r  }r  (h}r  (hhhhu}r	  (hhhhu�r
  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  uauuX   __dict__r  }r  (hh;h	}r  h=]r  h?X   mappingproxyr  �r  asuX   __hash__r  }r  (hh�h	}r  (hX   Return hash(self).r  h]r  }r  (h}r  (hhhhu}r  (hhhhu�r  hX   Return hash(self).r  uauuX	   find_specr  }r  (hh;h	}r  h=]r  h?X   methodr   �r!  asuX
   get_sourcer"  }r#  (hhh	}r$  (hX8   Return None as built-in modules do not have source code.r%  h]r&  }r'  (h}r(  (hhhhu}r)  (hhhhu�r*  hX8   Return None as built-in modules do not have source code.r+  uauuX   __weakref__r,  }r-  (hX   propertyr.  h	}r/  (hX2   list of weak references to the object (if defined)r0  h=]r1  h�auuX	   __class__r2  }r3  (hh*h	]r4  h?X   typer5  �r6  auX
   is_packager7  }r8  (hhh	}r9  (hX4   Return False as built-in modules are never packages.r:  h]r;  }r<  (h}r=  (hhhhu}r>  (hhhhu�r?  hX4   Return False as built-in modules are never packages.r@  uauuX   get_coderA  }rB  (hhh	}rC  (hX9   Return None as built-in modules do not have code objects.rD  h]rE  }rF  (h}rG  (hhhhu}rH  (hhhhu�rI  hX9   Return None as built-in modules do not have code objects.rJ  uauuX   create_modulerK  }rL  (hhh	}rM  (hX   Create a built-in modulerN  h]rO  }rP  (h}rQ  (hhhhu}rR  (hhhhu�rS  hX   Create a built-in modulerT  uauuX
   __sizeof__rU  }rV  (hh�h	}rW  (hX6   __sizeof__() -> int
size of object in memory, in bytesrX  h]rY  }rZ  (h}r[  (h=]r\  h?X   objectr]  �r^  ahX   selfr_  u�r`  hX"   size of object in memory, in bytesra  hp]rb  h?X   intrc  �rd  auauuX
   __reduce__re  }rf  (hh�h	}rg  (hX   helper for picklerh  h]ri  }rj  (h}rk  (hhhhu}rl  (hhhhu�rm  hX   helper for picklern  uauuX   __delattr__ro  }rp  (hh�h	}rq  (hX   Implement delattr(self, name).rr  h]rs  }rt  (h}ru  (hhhhu}rv  (hhhhu�rw  hX   Implement delattr(self, name).rx  uauuX   __eq__ry  }rz  (hh�h	}r{  (hX   Return self==value.r|  h]r}  }r~  (h}r  (hhhhu}r�  (hhhhu�r�  hX   Return self==value.r�  uauuhZ}r�  (hh;h	}r�  h=]r�  hAasuX   __ne__r�  }r�  (hh�h	}r�  (hX   Return self!=value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX   Return self!=value.r�  uauuX   __dir__r�  }r�  (hh�h	}r�  (hX.   __dir__() -> list
default dir() implementationr�  h]r�  }r�  (h}r�  (h=]r�  j^  ahj_  u�r�  hX   default dir() implementationr�  hp]r�  h?X   listr�  �r�  auauuX   __setattr__r�  }r�  (hh�h	}r�  (hX%   Implement setattr(self, name, value).r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX%   Implement setattr(self, name, value).r�  uauuX   __repr__r�  }r�  (hh�h	}r�  (hX   Return repr(self).r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX   Return repr(self).r�  uauuX   load_moduler�  }r�  (hhh	}r�  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  uauuX
   __module__r�  }r�  (hh;h	}r�  h=]r�  hAasuX   __init_subclass__r�  }r�  (hhh	}r�  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  uauuX   __ge__r�  }r�  (hh�h	}r�  (hX   Return self>=value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX   Return self>=value.r�  uauuX   __init__r�  }r�  (hh�h	}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX>   Initialize self.  See help(type(self)) for accurate signature.r�  uauuX   __reduce_ex__r�  }r�  (hh�h	}r�  (hX   helper for pickler�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX   helper for pickler�  uauuX   __subclasshook__r�  }r�  (hhh	}r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  uauuX   __le__r�  }r�  (hh�h	}r�  (hX   Return self<=value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhhhu�r�  hX   Return self<=value.r�  uauuX   __gt__r�  }r�  (hh�h	}r�  (hX   Return self>value.r�  h]r�  }r   (h}r  (hhhhu}r  (hhhhu�r  hX   Return self>value.r  uauuX   __new__r  }r  (hhh	}r  (hXG   Create and return a new object.  See help(type) for accurate signature.r  h]r	  }r
  (h}r  (hhhhu}r  (hhhhu�r  hXG   Create and return a new object.  See help(type) for accurate signature.r  uauuuuuuu.