�}q (X   docqXO  zipimport provides support for importing Python modules from Zip archives.

This module exports three objects:
- zipimporter: a class; its constructor takes a path to a Zip archive.
- ZipImportError: exception raised by zipimporter objects. It's a
  subclass of ImportError, so it can be caught as ImportError, too.
- _zip_directory_cache: a dict, mapping archive paths to zip directory
  info dicts, as used in zipimporter._files.

It is usually not needed to use the zipimport module explicitly; it is
used by the builtin import mechanism for sys.path items that are paths
to Zip archives.qX   membersq}q(X
   __loader__q}q(X   kindqX   typerefqX   valueq	]q
X   _frozen_importlibqX   BuiltinImporterq�qauX   zipimporterq}q(hX   typeqh	}q(X   mroq]q(X	   zipimportqX   zipimporterq�qX   builtinsqX   objectq�qeX   basesq]qhahX�  zipimporter(archivepath) -> zipimporter object

Create a new zipimporter instance. 'archivepath' must be a path to
a zipfile, or to a specific path inside a zipfile. For example, it can be
'/tmp/myimport.zip', or '/tmp/myimport.zip/mydirectory', if mydirectory is a
valid directory inside the archive.

'ZipImportError is raised if 'archivepath' doesn't point to a valid Zip
archive.

The 'archive' attribute of zipimporter objects contains the name of the
zipfile targeted.qh}q(X   _filesq}q(hX   propertyq h	}q!h]q"hasuX
   __sizeof__q#}q$(hX   methodq%h	}q&(hX6   __sizeof__() -> int
size of object in memory, in bytesq'X	   overloadsq(]q)}q*(X   argsq+}q,(h]q-hX   objectq.�q/aX   nameq0X   selfq1u�q2hX"   size of object in memory, in bytesq3X   ret_typeq4]q5hX   intq6�q7auauuX
   __format__q8}q9(hh%h	}q:(hX   default object formatterq;h(]q<}q=(h+}q>(h0h+X
   arg_formatq?X   *q@u}qA(h0X   kwargsqBh?X   **qCu�qDhX   default object formatterqEuauuX   __lt__qF}qG(hh%h	}qH(hX   Return self<value.qIh(]qJ}qK(h+}qL(h0h+h?h@u}qM(h0hBh?hCu�qNhX   Return self<value.qOuauuX
   __reduce__qP}qQ(hh%h	}qR(hX   helper for pickleqSh(]qT}qU(h+}qV(h0h+h?h@u}qW(h0hBh?hCu�qXhX   helper for pickleqYuauuX   find_loaderqZ}q[(hh%h	}q\(hX�  find_loader(fullname, path=None) -> self, str or None.

Search for a module specified by 'fullname'. 'fullname' must be the
fully qualified (dotted) module name. It returns the zipimporter
instance itself if the module was found, a string containing the
full path name if it's possibly a portion of a namespace package,
or None otherwise. The optional 'path' argument is ignored -- it's
 there for compatibility with the importer protocol.q]h(]q^}q_(h+}q`(h]qah/ah0h1u}qbh0X   fullnameqcs}qd(h0X   pathqeX   default_valueqfX   Noneqgu�qhhX�  self, str or None.

Search for a module specified by 'fullname'. 'fullname' must be the
fully qualified (dotted) module name. It returns the zipimporter
instance itself if the module was found, a string containing the
full path name if it's possibly a portion of a namespace package,
or None otherwise. The optional 'path' argument is ignored -- it's
 there for compatibility with the importer protocol.qih4]qjX    qkhk�qlauauuX   __delattr__qm}qn(hh%h	}qo(hX   Implement delattr(self, name).qph(]qq}qr(h+}qs(h0h+h?h@u}qt(h0hBh?hCu�quhX   Implement delattr(self, name).qvuauuX   __str__qw}qx(hh%h	}qy(hX   Return str(self).qzh(]q{}q|(h+}q}(h0h+h?h@u}q~(h0hBh?hCu�qhX   Return str(self).q�uauuX   __eq__q�}q�(hh%h	}q�(hX   Return self==value.q�h(]q�}q�(h+}q�(h0h+h?h@u}q�(h0hBh?hCu�q�hX   Return self==value.q�uauuX   find_moduleq�}q�(hh%h	}q�(hXZ  find_module(fullname, path=None) -> self or None.

Search for a module specified by 'fullname'. 'fullname' must be the
fully qualified (dotted) module name. It returns the zipimporter
instance itself if the module was found, or None if it wasn't.
The optional 'path' argument is ignored -- it's there for compatibility
with the importer protocol.q�h(]q�}q�(h+}q�(h]q�h/ah0h1u}q�h0X   fullnameq�s}q�(h0X   pathq�hfX   Noneq�u�q�hX*  .

Search for a module specified by 'fullname'. 'fullname' must be the
fully qualified (dotted) module name. It returns the zipimporter
instance itself if the module was found, or None if it wasn't.
The optional 'path' argument is ignored -- it's there for compatibility
with the importer protocol.q�h4]q�hkX   selfq��q�auauuX   __doc__q�}q�(hX   dataq�h	}q�h]q�hX   strq��q�asuX   __hash__q�}q�(hh%h	}q�(hX   Return hash(self).q�h(]q�}q�(h+}q�(h0h+h?h@u}q�(h0hBh?hCu�q�hX   Return hash(self).q�uauuX   __ne__q�}q�(hh%h	}q�(hX   Return self!=value.q�h(]q�}q�(h+}q�(h0h+h?h@u}q�(h0hBh?hCu�q�hX   Return self!=value.q�uauuX   __dir__q�}q�(hh%h	}q�(hX.   __dir__() -> list
default dir() implementationq�h(]q�}q�(h+}q�(h]q�h/ah0h1u�q�hX   default dir() implementationq�h4]q�hX   listqÆq�auauuX   __setattr__q�}q�(hh%h	}q�(hX%   Implement setattr(self, name, value).q�h(]q�}q�(h+}q�(h0h+h?h@u}q�(h0hBh?hCu�q�hX%   Implement setattr(self, name, value).q�uauuX   __repr__q�}q�(hh%h	}q�(hX   Return repr(self).q�h(]q�}q�(h+}q�(h0h+h?h@u}q�(h0hBh?hCu�q�hX   Return repr(self).q�uauuX
   get_sourceq�}q�(hh%h	}q�(hX�   get_source(fullname) -> source string.

Return the source code for the specified module. Raise ZipImportError
if the module couldn't be found, return None if the archive does
contain the module, but has no source for it.q�h(]q�}q�(h+}q�(h]q�h/ah0h1u}q�h0X   fullnameq�s�q�hX�   .

Return the source code for the specified module. Raise ZipImportError
if the module couldn't be found, return None if the archive does
contain the module, but has no source for it.q�h4]q�hX   strq�q�auauuX   load_moduleq�}q�(hh%h	}q�(hX�   load_module(fullname) -> module.

Load the module specified by 'fullname'. 'fullname' must be the
fully qualified (dotted) module name. It returns the imported
module, or raises ZipImportError if it wasn't found.q�h(]q�}q�(h+}q�(h]q�h/ah0h1u}q�h0X   fullnameq�s�q�hX�   .

Load the module specified by 'fullname'. 'fullname' must be the
fully qualified (dotted) module name. It returns the imported
module, or raises ZipImportError if it wasn't found.q�h4]q�hX   moduleq��q�auauuX   archiveq�}q�(hh h	}q�h]q�hasuX   __init_subclass__q�}q�(hX   functionq�h	}q�(hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
q�h(]r   }r  (h+}r  (h0h+h?h@u}r  (h0hBh?hCu�r  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r  uauuX   prefixr  }r  (hh h	}r  h]r	  hasuX   __ge__r
  }r  (hh%h	}r  (hX   Return self>=value.r  h(]r  }r  (h+}r  (h0h+h?h@u}r  (h0hBh?hCu�r  hX   Return self>=value.r  uauuX   get_datar  }r  (hh%h	}r  (hX�   get_data(pathname) -> string with file data.

Return the data associated with 'pathname'. Raise IOError if
the file wasn't found.r  h(]r  }r  (h+}r  (h]r  h/ah0h1u}r  h0X   pathnamer  s�r  hXV   .

Return the data associated with 'pathname'. Raise IOError if
the file wasn't found.r  h4]r   h�auauuX	   __class__r!  }r"  (hhh	]r#  hX   typer$  �r%  auX   __init__r&  }r'  (hh%h	}r(  (hX>   Initialize self.  See help(type(self)) for accurate signature.r)  h(]r*  }r+  (h+}r,  (h0h+h?h@u}r-  (h0hBh?hCu�r.  hX>   Initialize self.  See help(type(self)) for accurate signature.r/  uauuX   __reduce_ex__r0  }r1  (hh%h	}r2  (hX   helper for pickler3  h(]r4  }r5  (h+}r6  (h0h+h?h@u}r7  (h0hBh?hCu�r8  hX   helper for pickler9  uauuX   __subclasshook__r:  }r;  (hh�h	}r<  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r=  h(]r>  }r?  (h+}r@  (h0h+h?h@u}rA  (h0hBh?hCu�rB  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rC  uauuX   __le__rD  }rE  (hh%h	}rF  (hX   Return self<=value.rG  h(]rH  }rI  (h+}rJ  (h0h+h?h@u}rK  (h0hBh?hCu�rL  hX   Return self<=value.rM  uauuX
   is_packagerN  }rO  (hh%h	}rP  (hX�   is_package(fullname) -> bool.

Return True if the module specified by fullname is a package.
Raise ZipImportError if the module couldn't be found.rQ  h(]rR  }rS  (h+}rT  (h]rU  h/ah0h1u}rV  h0X   fullnamerW  s�rX  hXv   .

Return True if the module specified by fullname is a package.
Raise ZipImportError if the module couldn't be found.rY  h4]rZ  hX   boolr[  �r\  auauuX   __gt__r]  }r^  (hh%h	}r_  (hX   Return self>value.r`  h(]ra  }rb  (h+}rc  (h0h+h?h@u}rd  (h0hBh?hCu�re  hX   Return self>value.rf  uauuX   get_coderg  }rh  (hh%h	}ri  (hX�   get_code(fullname) -> code object.

Return the code object for the specified module. Raise ZipImportError
if the module couldn't be found.rj  h(]rk  }rl  (h+}rm  (h]rn  h/ah0h1u}ro  h0X   fullnamerp  s�rq  hXi   .

Return the code object for the specified module. Raise ZipImportError
if the module couldn't be found.rr  h4]rs  hX   codert  �ru  auauuX   get_filenamerv  }rw  (hh%h	}rx  (hXY   get_filename(fullname) -> filename string.

Return the filename for the specified module.ry  h(]rz  }r{  (h+}r|  (h]r}  h/ah0h1u}r~  h0X   fullnamer  s�r�  hX0   .

Return the filename for the specified module.r�  h4]r�  h�auauuX   __new__r�  }r�  (hh�h	}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hXG   Create and return a new object.  See help(type) for accurate signature.r�  uauuuuuX   ZipImportErrorr�  }r�  (hhh	}r�  (h]r�  (X	   zipimportr�  X   ZipImportErrorr�  �r�  hX   ImportErrorr�  �r�  hX	   Exceptionr�  �r�  hX   BaseExceptionr�  �r�  heh]r�  j�  ah}r�  (h8}r�  (hh%h	}r�  (hX   default object formatterr�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   default object formatterr�  uauuhF}r�  (hh%h	}r�  (hX   Return self<value.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return self<value.r�  uauuhw}r�  (hh%h	}r�  (hX   Return str(self).r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return str(self).r�  uauuX   msgr�  }r�  (hh h	}r�  (hX   exception messager�  h]r�  hauuX   __dict__r�  }r�  (hh�h	}r�  h]r�  hX   mappingproxyr�  �r�  asuh�}r�  (hh%h	}r�  (hX   Return hash(self).r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return hash(self).r�  uauuX   __context__r�  }r�  (hh h	}r�  (hX   exception contextr�  h]r�  hauuX   __weakref__r�  }r�  (hh h	}r�  (hX2   list of weak references to the object (if defined)r�  h]r�  hauuj!  }r�  (hhh	]r�  j%  auh0}r�  (hh h	}r�  (hX   module namer�  h]r�  hauuX   pathr�  }r�  (hh h	}r�  (hX   module pathr�  h]r�  hauuX   __traceback__r�  }r�  (hh h	}r�  h]r�  hasuh#}r�  (hh%h	}r�  (hX6   __sizeof__() -> int
size of object in memory, in bytesr�  h(]r�  }r�  (h+}r�  (h]r�  h/ah0h1u�r�  hX"   size of object in memory, in bytesr�  h4]r�  h7auauuhP}r�  (hh%h	}r�  h(NsuX	   __cause__r�  }r�  (hh h	}r�  (hX   exception causer�  h]r�  hauuhm}r�  (hh%h	}r�  (hX   Implement delattr(self, name).r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Implement delattr(self, name).r�  uauuh+}r�  (hh h	}r�  h]r   hasuh�}r  (hh%h	}r  (hX   Return self==value.r  h(]r  }r  (h+}r  (h0h+h?h@u}r  (h0hBh?hCu�r  hX   Return self==value.r	  uauuh�}r
  (hh�h	}r  h]r  hX   NoneTyper  �r  asuh�}r  (hh%h	}r  (hX   Return self!=value.r  h(]r  }r  (h+}r  (h0h+h?h@u}r  (h0hBh?hCu�r  hX   Return self!=value.r  uauuh�}r  (hh%h	}r  (hX.   __dir__() -> list
default dir() implementationr  h(]r  }r  (h+}r  (h]r  h/ah0h1u�r  hX   default dir() implementationr   h4]r!  h�auauuh�}r"  (hh%h	}r#  (hX%   Implement setattr(self, name, value).r$  h(]r%  }r&  (h+}r'  (h0h+h?h@u}r(  (h0hBh?hCu�r)  hX%   Implement setattr(self, name, value).r*  uauuh�}r+  (hh%h	}r,  (hX   Return repr(self).r-  h(]r.  }r/  (h+}r0  (h0h+h?h@u}r1  (h0hBh?hCu�r2  hX   Return repr(self).r3  uauuX
   __module__r4  }r5  (hh�h	}r6  h]r7  h�asuh�}r8  (hh�h	}r9  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r:  h(]r;  }r<  (h+}r=  (h0h+h?h@u}r>  (h0hBh?hCu�r?  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r@  uauuj
  }rA  (hh%h	}rB  (hX   Return self>=value.rC  h(]rD  }rE  (h+}rF  (h0h+h?h@u}rG  (h0hBh?hCu�rH  hX   Return self>=value.rI  uauuX   __setstate__rJ  }rK  (hh%h	}rL  h(NsuX   __suppress_context__rM  }rN  (hh h	}rO  h]rP  hasuj&  }rQ  (hh%h	}rR  (hX>   Initialize self.  See help(type(self)) for accurate signature.rS  h(]rT  }rU  (h+}rV  (h0h+h?h@u}rW  (h0hBh?hCu�rX  hX>   Initialize self.  See help(type(self)) for accurate signature.rY  uauuj0  }rZ  (hh%h	}r[  (hX   helper for pickler\  h(]r]  }r^  (h+}r_  (h0h+h?h@u}r`  (h0hBh?hCu�ra  hX   helper for picklerb  uauuj:  }rc  (hh�h	}rd  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
re  h(]rf  }rg  (h+}rh  (h0h+h?h@u}ri  (h0hBh?hCu�rj  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rk  uauujD  }rl  (hh%h	}rm  (hX   Return self<=value.rn  h(]ro  }rp  (h+}rq  (h0h+h?h@u}rr  (h0hBh?hCu�rs  hX   Return self<=value.rt  uauuX   with_tracebackru  }rv  (hh%h	}rw  (hXQ   Exception.with_traceback(tb) --
    set self.__traceback__ to tb and return self.rx  h(]ry  }rz  (h+}r{  (h]r|  h/ah0h1u}r}  h0X   tbr~  s�r  hX-   set self.__traceback__ to tb and return self.r�  uauuj]  }r�  (hh%h	}r�  (hX   Return self>value.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return self>value.r�  uauuj�  }r�  (hh�h	}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hXG   Create and return a new object.  See help(type) for accurate signature.r�  uauuuuuX   __name__r�  }r�  (hh�h	}r�  h]r�  h�asuX   _zip_directory_cacher�  }r�  (hh�h	}r�  h]r�  hX   dictr�  �r�  asuh�}r�  (hh�h	}r�  h]r�  h�asuX   __spec__r�  }r�  (hh�h	}r�  h]r�  hX
   ModuleSpecr�  �r�  asuX   __package__r�  }r�  (hh�h	}r�  h]r�  h�asuh}r�  (hhh	}r�  (h]r�  (hheh]r�  hahX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X	   is_hiddenr�  �h}r�  (X   module_reprr�  }r�  (hh�h	}r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  uauuh8}r�  (hh%h	}r�  (hX   default object formatterr�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   default object formatterr�  uauuhF}r�  (hh%h	}r�  (hX   Return self<value.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return self<value.r�  uauuX   exec_moduler�  }r�  (hh�h	}r�  (hX   Exec a built-in moduler�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Exec a built-in moduler�  uauuhw}r�  (hh%h	}r�  (hX   Return str(self).r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return str(self).r�  uauuh�}r�  (hh�h	}r�  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  uauuj�  }r�  (hh�h	}r�  h]r�  j�  asuh�}r�  (hh%h	}r�  (hX   Return hash(self).r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return hash(self).r�  uauuX	   find_specr�  }r�  (hh�h	}r�  h]r�  hX   methodr�  �r�  asuh�}r�  (hh�h	}r�  (hX8   Return None as built-in modules do not have source code.r�  h(]r�  }r�  (h+}r   (h0h+h?h@u}r  (h0hBh?hCu�r  hX8   Return None as built-in modules do not have source code.r  uauuj�  }r  (hh h	}r  (hX2   list of weak references to the object (if defined)r  h]r  hauuj!  }r  (hhh	]r	  j%  aujN  }r
  (hh�h	}r  (hX4   Return False as built-in modules are never packages.r  h(]r  }r  (h+}r  (h0h+h?h@u}r  (h0hBh?hCu�r  hX4   Return False as built-in modules are never packages.r  uauujg  }r  (hh�h	}r  (hX9   Return None as built-in modules do not have code objects.r  h(]r  }r  (h+}r  (h0h+h?h@u}r  (h0hBh?hCu�r  hX9   Return None as built-in modules do not have code objects.r  uauuX   create_moduler  }r  (hh�h	}r  (hX   Create a built-in moduler  h(]r   }r!  (h+}r"  (h0h+h?h@u}r#  (h0hBh?hCu�r$  hX   Create a built-in moduler%  uauuh#}r&  (hh%h	}r'  (hX6   __sizeof__() -> int
size of object in memory, in bytesr(  h(]r)  }r*  (h+}r+  (h]r,  h/ah0h1u�r-  hX"   size of object in memory, in bytesr.  h4]r/  h7auauuhP}r0  (hh%h	}r1  (hX   helper for pickler2  h(]r3  }r4  (h+}r5  (h0h+h?h@u}r6  (h0hBh?hCu�r7  hX   helper for pickler8  uauuhm}r9  (hh%h	}r:  (hX   Implement delattr(self, name).r;  h(]r<  }r=  (h+}r>  (h0h+h?h@u}r?  (h0hBh?hCu�r@  hX   Implement delattr(self, name).rA  uauuh�}rB  (hh%h	}rC  (hX   Return self==value.rD  h(]rE  }rF  (h+}rG  (h0h+h?h@u}rH  (h0hBh?hCu�rI  hX   Return self==value.rJ  uauuh�}rK  (hh�h	}rL  h]rM  h�asuh�}rN  (hh%h	}rO  (hX   Return self!=value.rP  h(]rQ  }rR  (h+}rS  (h0h+h?h@u}rT  (h0hBh?hCu�rU  hX   Return self!=value.rV  uauuh�}rW  (hh%h	}rX  (hX.   __dir__() -> list
default dir() implementationrY  h(]rZ  }r[  (h+}r\  (h]r]  h/ah0h1u�r^  hX   default dir() implementationr_  h4]r`  h�auauuh�}ra  (hh%h	}rb  (hX%   Implement setattr(self, name, value).rc  h(]rd  }re  (h+}rf  (h0h+h?h@u}rg  (h0hBh?hCu�rh  hX%   Implement setattr(self, name, value).ri  uauuh�}rj  (hh%h	}rk  (hX   Return repr(self).rl  h(]rm  }rn  (h+}ro  (h0h+h?h@u}rp  (h0hBh?hCu�rq  hX   Return repr(self).rr  uauuh�}rs  (hh�h	}rt  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    ru  h(]rv  }rw  (h+}rx  (h0h+h?h@u}ry  (h0hBh?hCu�rz  hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r{  uauuj4  }r|  (hh�h	}r}  h]r~  h�asuh�}r  (hh�h	}r�  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  uauuj
  }r�  (hh%h	}r�  (hX   Return self>=value.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return self>=value.r�  uauuj&  }r�  (hh%h	}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX>   Initialize self.  See help(type(self)) for accurate signature.r�  uauuj0  }r�  (hh%h	}r�  (hX   helper for pickler�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   helper for pickler�  uauuj:  }r�  (hh�h	}r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  uauujD  }r�  (hh%h	}r�  (hX   Return self<=value.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return self<=value.r�  uauuj]  }r�  (hh%h	}r�  (hX   Return self>value.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hX   Return self>value.r�  uauuj�  }r�  (hh�h	}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h(]r�  }r�  (h+}r�  (h0h+h?h@u}r�  (h0hBh?hCu�r�  hXG   Create and return a new object.  See help(type) for accurate signature.r�  uauuuuuuu.