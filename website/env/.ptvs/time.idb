�}q (X   docqX  This module provides various functions to manipulate time values.

There are two standard representations of time.  One is the number
of seconds since the Epoch, in UTC (a.k.a. GMT).  It may be an integer
or a floating point number (to represent fractions of seconds).
The Epoch is system-defined; on Unix, it is generally January 1st, 1970.
The actual value can be retrieved by calling gmtime(0).

The other representation is a tuple of 9 integers giving local time.
The tuple items are:
  year (including century, e.g. 1998)
  month (1-12)
  day (1-31)
  hours (0-23)
  minutes (0-59)
  seconds (0-59)
  weekday (0-6, Monday is 0)
  Julian day (day in the year, 1-366)
  DST (Daylight Savings Time) flag (-1, 0 or 1)
If the DST flag is 0, the time is given in the regular time zone;
if it is 1, the time is given in the DST time zone;
if it is -1, mktime() should guess based on the date and time.

Variables:

timezone -- difference in seconds between UTC and local standard time
altzone -- difference in  seconds between UTC and local DST time
daylight -- whether local time should reflect DST
tzname -- tuple of (standard time zone name, DST time zone name)

Functions:

time() -- return current time in seconds since the Epoch as a float
clock() -- return CPU time since process start as a float
sleep() -- delay for a number of seconds given as a float
gmtime() -- convert seconds since Epoch to UTC tuple
localtime() -- convert seconds since Epoch to local time tuple
asctime() -- convert time tuple to string
ctime() -- convert time in seconds to string
mktime() -- convert local time tuple to seconds since Epoch
strftime() -- convert time tuple to string according to format specification
strptime() -- parse string to time tuple according to format specification
tzset() -- change the local timezoneqX   membersq}q(X   perf_counterq}q(X   kindqX   functionqX   valueq	}q
(hX>   perf_counter() -> float

Performance counter for benchmarking.qX	   overloadsq]q}q(X   argsq)hX%   Performance counter for benchmarking.qX   ret_typeq]qX   builtinsqX   floatq�qauauuX   strptimeq}q(hhh	}q(hX�  strptime(string, format) -> struct_time

Parse a string to a time tuple according to a format specification.
See the library reference manual for formatting codes (same as
strftime()).

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
qh]q(}q(h}qX   nameqX   stringqs}qhX   formatq s�q!hXb  Parse a string to a time tuple according to a format specification.
See the library reference manual for formatting codes (same as
strftime()).

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
q"h]q#X   timeq$X   struct_timeq%�q&au}q'(X   ret_typeq(]q)X   __builtin__q*X   objectq+�q,aX   argsq-}q.(X   typeq/]q0h*X   strq1�q2aX   nameq3X   stringq4u�q5u}q6(h(]q7h,ah-}q8(h/]q9h2ah3X   stringq:u}q;(h/]q<h2ah3X   formatq=u�q>ueuuX   struct_timeq?}q@(hX   typeqAh	}qB(X   mroqC]qD(X   timeqEX   struct_timeqF�qGhX   tupleqH�qIhX   objectqJ�qKeX   basesqL]qMhIahX�  The time value as returned by gmtime(), localtime(), and strptime(), and
 accepted by asctime(), mktime() and strftime().  May be considered as a
 sequence of 9 integers.

 Note that several fields' values are not the same as those defined by
 the C language standard for struct tm.  For example, the value of the
 field tm_year is the actual year, not year - 1900.  See individual
 fields' descriptions for details.qNh}qO(X   tm_wdayqP}qQ(hX   propertyqRh	}qS(hX&   day of week, range [0, 6], Monday is 0qThA]qU(hKh,euuX
   __sizeof__qV}qW(hX   methodqXh	}qY(hX6   __sizeof__() -> int
size of object in memory, in bytesqZh]q[(}q\(h}q](hA]q^hX   objectq_�q`ahX   selfqau�qbhX"   size of object in memory, in bytesqch]qdhX   intqe�qfau}qg(h(]qhh*X   intqi�qjah-}qk(h/]qlh,ah3X   selfqmu�qnueuuX   n_fieldsqo}qp(hX   dataqqh	}qrhA]qshX   intqt�quasuX   tm_secqv}qw(hhRh	}qx(hX   seconds, range [0, 61])qyhA]qz(hKh,euuX   __getitem__q{}q|(hhXh	}q}(hX   Return self[key].q~h]q(}q�(h}q�(hhX
   arg_formatq�X   *q�u}q�(hX   kwargsq�h�X   **q�u�q�hX   Return self[key].q�u}q�(h(]q�h,ah-}q�(h/]q�h*X   tupleq��q�ah3X   selfq�u}q�(h/]q�h*X   longq��q�ah3X   indexq�u�q�u}q�(h(]q�h,ah-}q�(h/]q�h�ah3h�u}q�(h/]q�h*X   sliceq��q�ah3X   sliceq�u�q�u}q�(h(]q�h,ah-}q�(h/]q�h�ah3h�u}q�(h/]q�hjah3X   indexq�u�q�u}q�(h(]q�h,ah-}q�(h/]q�h�ah3h�u}q�(h/]q�h,ah3X   indexq�u�q�ueuuX
   __format__q�}q�(hhXh	}q�(hX   default object formatterq�h]q�(}q�(h}q�(hhh�h�u}q�(hh�h�h�u�q�hX   default object formatterq�u}q�(h(]q�h2ah-}q�(h/]q�h,ah3X   selfq�u}q�(h/]q�h2ah3X
   formatSpecq�u�q�ueuuX   __iter__q�}q�(hhXh	}q�(hX   Implement iter(self).q�h]q�}q�(h}q�(hhh�h�u}q�(hh�h�h�u�q�hX   Implement iter(self).q�uauuX
   __reduce__q�}q�(hhXh	}q�(h]q�}q�(h(]q�h�ah-}q�(h/]q�X   timeq�X   struct_timeqֆq�ah3h�u�q�uahX   helper for pickleq�uuX   __lt__q�}q�(hhXh	}q�(hX   Return self<value.q�h]q�(}q�(h}q�(hhh�h�u}q�(hh�h�h�u�q�hX   Return self<value.q�u}q�(h(]q�h,ah-}q�(h/]q�h,ah3X   yq�u}q�(h/]q�h�ah3X   xq�u�q�u}q�(h(]q�h,ah-}q�(h/]q�h�ah3h�u}q�(h/]q�h,ah3h�u�q�u}q�(h(]q�h*X   boolq��q�ah-}q�(h/]q�h�ah3h�u}q�(h/]q�h�ah3h�u�q�ueuuX   __mul__q�}q�(hhXh	}q�(hX   Return self*value.nr   h]r  (}r  (h}r  (hhh�h�u}r  (hh�h�h�u�r  hX   Return self*value.nr  u}r  (h(]r  h,ah-}r	  (h/]r
  h�ah3X   selfr  u}r  (h/]r  h,ah3X   countr  u�r  u}r  (h(]r  h�ah-}r  (h/]r  h�ah3h�u}r  (h/]r  hjah3X   nr  u�r  ueuuX   __contains__r  }r  (hhXh	}r  (hX   Return key in self.r  h]r  }r  (h}r  (hhh�h�u}r  (hh�h�h�u�r   hX   Return key in self.r!  uauuX   __delattr__r"  }r#  (hhXh	}r$  (hX   Implement delattr(self, name).r%  h]r&  (}r'  (h}r(  (hhh�h�u}r)  (hh�h�h�u�r*  hX   Implement delattr(self, name).r+  u}r,  (h(]r-  h*X   NoneTyper.  �r/  ah-}r0  (h/]r1  h,ah3X   selfr2  u}r3  (h/]r4  h2ah3X   namer5  u�r6  ueuuX   __str__r7  }r8  (hhXh	}r9  (hX   Return str(self).r:  h]r;  (}r<  (h}r=  (hhh�h�u}r>  (hh�h�h�u�r?  hX   Return str(self).r@  u}rA  (h(]rB  h2ah-}rC  (h/]rD  h,ah3X   selfrE  u�rF  ueuuX   __eq__rG  }rH  (hhXh	}rI  (hX   Return self==value.rJ  h]rK  (}rL  (h}rM  (hhh�h�u}rN  (hh�h�h�u�rO  hX   Return self==value.rP  u}rQ  (h(]rR  h,ah-}rS  (h/]rT  h,ah3h�u}rU  (h/]rV  h�ah3h�u�rW  u}rX  (h(]rY  h,ah-}rZ  (h/]r[  h�ah3h�u}r\  (h/]r]  h,ah3h�u�r^  u}r_  (h(]r`  h�ah-}ra  (h/]rb  h�ah3h�u}rc  (h/]rd  h�ah3h�u�re  ueuuX   __rmul__rf  }rg  (hhXh	}rh  (hX   Return self*value.ri  h]rj  (}rk  (h}rl  (hhh�h�u}rm  (hh�h�h�u�rn  hX   Return self*value.ro  u}rp  (h(]rq  h,ah-}rr  (h/]rs  h,ah3X   countrt  u}ru  (h/]rv  h�ah3X   selfrw  u�rx  u}ry  (h(]rz  h�ah-}r{  (h/]r|  hjah3j  u}r}  (h/]r~  h�ah3h�u�r  ueuuX   __doc__r�  }r�  (hhqh	}r�  hA]r�  (hX   strr�  �r�  h2esuX   __hash__r�  }r�  (hhXh	}r�  (hX   Return hash(self).r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return hash(self).r�  uauuX   __ne__r�  }r�  (hhXh	}r�  (hX   Return self!=value.r�  h]r�  (}r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return self!=value.r�  u}r�  (h(]r�  h,ah-}r�  (h/]r�  h,ah3h�u}r�  (h/]r�  h�ah3h�u�r�  u}r�  (h(]r�  h,ah-}r�  (h/]r�  h�ah3h�u}r�  (h/]r�  h,ah3h�u�r�  u}r�  (h(]r�  h�ah-}r�  (h/]r�  h�ah3h�u}r�  (h/]r�  h�ah3h�u�r�  ueuuX   __dir__r�  }r�  (hhXh	}r�  (hX.   __dir__() -> list
default dir() implementationr�  h]r�  }r�  (h}r�  (hA]r�  h`ahhau�r�  hX   default dir() implementationr�  h]r�  hX   listr�  �r�  auauuX   __setattr__r�  }r�  (hhXh	}r�  (hX%   Implement setattr(self, name, value).r�  h]r�  (}r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX%   Implement setattr(self, name, value).r�  u}r�  (h(]r�  j/  ah-}r�  (h/]r�  h,ah3X   selfr�  u}r�  (h/]r�  h2ah3X   namer�  u}r�  (h/]r�  h,ah3X   valuer�  u�r�  ueuuX	   tm_gmtoffr�  }r�  (hhRh	}r�  (hX   offset from UTC in secondsr�  hA]r�  hKauuX   __repr__r�  }r�  (hhXh	}r�  (hX   Return repr(self).r�  h]r�  (}r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return repr(self).r�  u}r�  (h(]r�  h2ah-}r�  (h/]r�  h�ah3h�u�r�  ueuuX   __add__r�  }r�  (hhXh	}r�  (hX   Return self+value.r�  h]r�  (}r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return self+value.r�  u}r�  (h(]r�  h�ah-}r�  (h/]r�  h�ah3h�u}r�  (h/]r�  h�ah3h�u�r�  ueuuX   tm_yearr�  }r�  (hhRh	}r�  (hX   year, for example, 1993r�  hA]r�  (hKh,euuX   tm_minr�  }r�  (hhRh	}r�  (hX   minutes, range [0, 59]r�  hA]r   (hKh,euuX   tm_zoner  }r  (hhRh	}r  (hX   abbreviation of timezone namer  hA]r  hKauuX   tm_hourr  }r  (hhRh	}r  (hX   hours, range [0, 23]r	  hA]r
  (hKh,euuX   tm_ydayr  }r  (hhRh	}r  (hX   day of year, range [1, 366]r  hA]r  (hKh,euuX   countr  }r  (hhXh	}r  (hXB   T.count(value) -> integer -- return number of occurrences of valuer  h]r  (}r  (h}r  (hA]r  h`ahhau}r  hX   valuer  s�r  hX    r  h]r  hfau}r  (h(]r  hjah-}r  (h/]r   h�ah3h�u}r!  (h/]r"  h,ah3X   objr#  u�r$  ueuuX   tm_isdstr%  }r&  (hhRh	}r'  (hX:   1 if summer time is in effect, 0 if not, and -1 if unknownr(  hA]r)  (hKh,euuX   __init_subclass__r*  }r+  (hhh	}r,  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r-  h]r.  }r/  (h}r0  (hhh�h�u}r1  (hh�h�h�u�r2  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r3  uauuX   n_sequence_fieldsr4  }r5  (hhqh	}r6  hA]r7  huasuX   __ge__r8  }r9  (hhXh	}r:  (hX   Return self>=value.r;  h]r<  (}r=  (h}r>  (hhh�h�u}r?  (hh�h�h�u�r@  hX   Return self>=value.rA  u}rB  (h(]rC  h,ah-}rD  (h/]rE  h,ah3h�u}rF  (h/]rG  h�ah3h�u�rH  u}rI  (h(]rJ  h,ah-}rK  (h/]rL  h�ah3h�u}rM  (h/]rN  h,ah3h�u�rO  u}rP  (h(]rQ  h�ah-}rR  (h/]rS  h�ah3h�u}rT  (h/]rU  h�ah3h�u�rV  ueuuX   n_unnamed_fieldsrW  }rX  (hhqh	}rY  hA]rZ  huasuX   __len__r[  }r\  (hhXh	}r]  (hX   Return len(self).r^  h]r_  (}r`  (h}ra  (hhh�h�u}rb  (hh�h�h�u�rc  hX   Return len(self).rd  u}re  (h(]rf  hjah-}rg  (h/]rh  h�ah3h�u�ri  ueuuX	   __class__rj  }rk  (hX   typerefrl  h	]rm  hX   typern  �ro  auX   __init__rp  }rq  (hhXh	}rr  (hX>   Initialize self.  See help(type(self)) for accurate signature.rs  h]rt  (}ru  (h}rv  (hhh�h�u}rw  (hh�h�h�u�rx  hX>   Initialize self.  See help(type(self)) for accurate signature.ry  u}rz  (h(]r{  j/  ah-}r|  (h/]r}  h,ah3X   selfr~  u}r  (X
   arg_formatr�  X   **r�  h/]r�  h*X   dictr�  �r�  ah3X   kwargsr�  u}r�  (j�  h�h/]r�  h�ah3X   argsr�  u�r�  u}r�  (h(]r�  j/  ah-}r�  (h/]r�  h,ah3X   selfr�  u}r�  (j�  h�h/]r�  h�ah3X   argsr�  u�r�  u}r�  (h(]r�  j/  ah-}r�  (h/]r�  h,ah3X   selfr�  u�r�  ueuuX   tm_monr�  }r�  (hhRh	}r�  (hX   month of year, range [1, 12]r�  hA]r�  (hKh,euuX   tm_mdayr�  }r�  (hhRh	}r�  (hX   day of month, range [1, 31]r�  hA]r�  (hKh,euuX   indexr�  }r�  (hhXh	}r�  (hXy   T.index(value, [start, [stop]]) -> integer -- return first index of value.
Raises ValueError if the value is not present.r�  h]r�  (}r�  (h(}r�  (hA]r�  h`ahhau}r�  hX   valuer�  s}r�  (hX   startr�  X   default_valuer�  X   Noner�  u}r�  (hX   stopr�  j�  j�  utr�  hX0   .
Raises ValueError if the value is not present.r�  h]r�  hfau}r�  (h(]r�  hjah-(}r�  (h/]r�  h�ah3h�u}r�  (h/]r�  h,ah3X   objr�  u}r�  (h/]r�  h,ah3X   startr�  u}r�  (h/]r�  h,ah3X   endr�  utr�  u}r�  (h(]r�  hjah-(}r�  (h/]r�  h�ah3h�u}r�  (h/]r�  h,ah3X   objr�  u}r�  (h/]r�  hjah3X   startr�  u}r�  (h/]r�  hjah3X   endr�  utr�  u}r�  (h(]r�  hjah-}r�  (h/]r�  h�ah3h�u}r�  (h/]r�  h,ah3X   objr�  u}r�  (h/]r�  h,ah3X   startr�  u�r�  u}r�  (h(]r�  hjah-}r�  (h/]r�  h�ah3h�u}r�  (h/]r�  h,ah3X   objr�  u}r�  (h/]r�  hjah3X   startr�  X   default_valuer�  X   0r�  u�r�  ueuuX   __reduce_ex__r�  }r�  (hhXh	}r�  (hX   helper for pickler�  h]r�  (}r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   helper for pickler�  u}r�  (h(]r�  h,ah-}r�  (h/]r�  h,ah3X   selfr�  u}r�  (h/]r�  h,ah3X   protocolr�  u�r�  u}r�  (h(]r�  h,ah-}r�  (h/]r   h,ah3X   selfr  u�r  ueuuX   __subclasshook__r  }r  (hhh	}r  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r  h]r  }r  (h}r	  (hhh�h�u}r
  (hh�h�h�u�r  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r  uauuX   __le__r  }r  (hhXh	}r  (hX   Return self<=value.r  h]r  (}r  (h}r  (hhh�h�u}r  (hh�h�h�u�r  hX   Return self<=value.r  u}r  (h(]r  h,ah-}r  (h/]r  h,ah3h�u}r  (h/]r  h�ah3h�u�r  u}r  (h(]r  h,ah-}r   (h/]r!  h�ah3h�u}r"  (h/]r#  h,ah3h�u�r$  u}r%  (h(]r&  h�ah-}r'  (h/]r(  h�ah3h�u}r)  (h/]r*  h�ah3h�u�r+  ueuuX   __gt__r,  }r-  (hhXh	}r.  (hX   Return self>value.r/  h]r0  (}r1  (h}r2  (hhh�h�u}r3  (hh�h�h�u�r4  hX   Return self>value.r5  u}r6  (h(]r7  h,ah-}r8  (h/]r9  h,ah3h�u}r:  (h/]r;  h�ah3h�u�r<  u}r=  (h(]r>  h,ah-}r?  (h/]r@  h�ah3h�u}rA  (h/]rB  h,ah3h�u�rC  u}rD  (h(]rE  h�ah-}rF  (h/]rG  h�ah3h�u}rH  (h/]rI  h�ah3h�u�rJ  ueuuX   __new__rK  }rL  (hhh	}rM  (hXG   Create and return a new object.  See help(type) for accurate signature.rN  h]rO  (}rP  (h}rQ  (hhh�h�u}rR  (hh�h�h�u�rS  hXG   Create and return a new object.  See help(type) for accurate signature.rT  u}rU  (h(]rV  h�ah-}rW  (h/]rX  h*X   typerY  �rZ  ah3X   clsr[  u}r\  (h/]r]  h�ah3X   sequencer^  u�r_  u}r`  (h(]ra  h�ah-(}rb  (h/]rc  jZ  ah3X   clsrd  u}re  (h/]rf  hjah3X   yearrg  u}rh  (h/]ri  hjah3X   monthrj  u}rk  (h/]rl  hjah3X   dayrm  u}rn  (h/]ro  hjah3X   hourrp  u}rq  (h/]rr  hjah3X   minuters  u}rt  (h/]ru  hjah3X   secondrv  u}rw  (h/]rx  hjah3X	   dayOfWeekry  u}rz  (h/]r{  hjah3X	   dayOfYearr|  u}r}  (h/]r~  hjah3X   isDstr  utr�  ueuuX   __getnewargs__r�  }r�  (hhXh	}r�  hNsuuuuX   process_timer�  }r�  (hhh	}r�  (hX_   process_time() -> float

Process time for profiling: sum of the kernel and user-space CPU time.r�  h]r�  }r�  (h)hXF   Process time for profiling: sum of the kernel and user-space CPU time.r�  h]r�  hauauuX   get_clock_infor�  }r�  (hhh	}r�  (hXJ   get_clock_info(name: str) -> dict

Get information of the specified clock.r�  h]r�  }r�  (h}r�  hX   namer�  s�r�  hX'   Get information of the specified clock.r�  h]r�  hX   dictr�  �r�  auauuX   _STRUCT_TM_ITEMSr�  }r�  (hhqh	}r�  hA]r�  huasuj�  }r�  (hhqh	}r�  hA]r�  (j�  h2esuX   mktimer�  }r�  (hhh	}r�  (hX(  mktime(tuple) -> floating point number

Convert a time tuple in local time to seconds since the Epoch.
Note that mktime(gmtime(0)) will not generally return zero for most
time zones; instead the returned value will either be equal to that
of the timezone or altzone attributes on the time module.r�  h]r�  (}r�  (h}r�  hX   tupler�  s�r�  hX   Convert a time tuple in local time to seconds since the Epoch.
Note that mktime(gmtime(0)) will not generally return zero for most
time zones; instead the returned value will either be equal to that
of the timezone or altzone attributes on the time module.r�  h]r�  hau}r�  (h(]r�  h*X   floatr�  �r�  ah-}r�  (h/]r�  h�ah3X	   localTimer�  u�r�  ueuuX   __package__r�  }r�  (hhqh	}r�  hA]r�  (j�  j/  esuX   sleepr�  }r�  (hhh	}r�  (hX�   sleep(seconds)

Delay execution for a given number of seconds.  The argument may be
a floating point number for subsecond precision.r�  h]r�  (}r�  (h}r�  hX   secondsr�  s�r�  hXt   Delay execution for a given number of seconds.  The argument may be
a floating point number for subsecond precision.r�  u}r�  (h(]r�  j/  ah-}r�  (h/]r�  j�  ah3X   tmr�  u�r�  ueuuX   gmtimer�  }r�  (hhh	}r�  (hXi  gmtime([seconds]) -> (tm_year, tm_mon, tm_mday, tm_hour, tm_min,
                       tm_sec, tm_wday, tm_yday, tm_isdst)

Convert seconds since the Epoch to a time tuple expressing UTC (a.k.a.
GMT).  When 'seconds' is not passed in, convert the current time instead.

If the platform supports the tm_gmtoff and tm_zone, they are available as
attributes only.r�  h]r�  (}r�  (h}r�  (hX   secondsr�  j�  j�  u�r�  hXT  (tm_year, tm_mon, tm_mday, tm_hour, tm_min,
                       tm_sec, tm_wday, tm_yday, tm_isdst)

Convert seconds since the Epoch to a time tuple expressing UTC (a.k.a.
GMT).  When 'seconds' is not passed in, convert the current time instead.

If the platform supports the tm_gmtoff and tm_zone, they are available as
attributes only.r�  h]r�  j  j  �r�  au}r�  (h(]r�  h�ah-)u}r�  (h(]r�  h�ah-}r�  (h/]r�  h,ah3X   secondsr�  u�r�  ueuuX	   monotonicr�  }r�  (hhh	}r�  (hX:   monotonic() -> float

Monotonic clock, cannot go backward.r�  h]r�  }r�  (h)hX$   Monotonic clock, cannot go backward.r�  h]r�  hauauuX
   __loader__r�  }r�  (hjl  h	]r�  X   _frozen_importlibr�  X   BuiltinImporterr�  �r�  auX   strftimer�  }r�  (hhh	}r�  (hX�  strftime(format[, tuple]) -> string

Convert a time tuple to a string according to a format specification.
See the library reference manual for formatting codes. When the time tuple
is not present, current time as returned by localtime() is used.

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
r�  h]r�  (}r�  (h}r�  hX   formatr�  s}r�  (hX   tupler�  j�  j�  u�r�  hX�  Convert a time tuple to a string according to a format specification.
See the library reference manual for formatting codes. When the time tuple
is not present, current time as returned by localtime() is used.

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
r�  h]r�  hX   strr�  �r�  au}r�  (h(]r�  h2ah-}r�  (h/]r�  h2ah3X   formatr�  u�r�  u}r�  (h(]r�  h2ah-}r   (h/]r  h2ah3X   formatr  u}r  (h/]r  h�ah3X   dateTimer  u�r  ueuuX   asctimer  }r  (hhh	}r	  (hX�   asctime([tuple]) -> string

Convert a time tuple to a string, e.g. 'Sat Jun 06 16:26:11 1998'.
When the time tuple is not present, current time as returned by localtime()
is used.r
  h]r  (}r  (h}r  (hX   tupler  j�  j�  u�r  hX�   Convert a time tuple to a string, e.g. 'Sat Jun 06 16:26:11 1998'.
When the time tuple is not present, current time as returned by localtime()
is used.r  h]r  j�  au}r  (h(]r  h2ah-)u}r  (h(]r  h2ah-}r  (h/]r  h,ah3X   timer  u�r  ueuuX   altzoner  }r  (hhqh	}r  hA]r  (huhjesuX   __name__r  }r  (hhqh	}r   hA]r!  (j�  h2esuX   daylightr"  }r#  (hhqh	}r$  hA]r%  (huhjesuhE}r&  (hhh	}r'  (hX�   time() -> floating point number

Return the current time in seconds since the Epoch.
Fractions of a second may be present if the system clock provides them.r(  h]r)  (}r*  (h)hX{   Return the current time in seconds since the Epoch.
Fractions of a second may be present if the system clock provides them.r+  h]r,  hau}r-  (h(]r.  j�  ah-)ueuuX   clockr/  }r0  (hhh	}r1  (hX�   clock() -> floating point number

Return the CPU time or real time since the start of the process or since
the first call to clock().  This has as much precision as the system
records.r2  h]r3  (}r4  (h)hX�   Return the CPU time or real time since the start of the process or since
the first call to clock().  This has as much precision as the system
records.r5  h]r6  hau}r7  (h(]r8  j�  ah-)ueuuX   timezoner9  }r:  (hhqh	}r;  hA]r<  (huhjesuX   ctimer=  }r>  (hhh	}r?  (hX�   ctime(seconds) -> string

Convert a time in seconds since the Epoch to a string in local time.
This is equivalent to asctime(localtime(seconds)). When the time tuple is
not present, current time as returned by localtime() is used.r@  h]rA  (}rB  (h}rC  hX   secondsrD  s�rE  hX�   Convert a time in seconds since the Epoch to a string in local time.
This is equivalent to asctime(localtime(seconds)). When the time tuple is
not present, current time as returned by localtime() is used.rF  h]rG  j�  au}rH  (h(]rI  h2ah-)u}rJ  (h(]rK  h2ah-}rL  (h/]rM  h,ah3X   secondsrN  u�rO  ueuuX   __spec__rP  }rQ  (hhqh	}rR  hA]rS  j�  X
   ModuleSpecrT  �rU  asuX   tznamerV  }rW  (hhqh	}rX  hA]rY  (hIh�esuX	   localtimerZ  }r[  (hhh	}r\  (hX  localtime([seconds]) -> (tm_year,tm_mon,tm_mday,tm_hour,tm_min,
                          tm_sec,tm_wday,tm_yday,tm_isdst)

Convert seconds since the Epoch to a time tuple expressing local time.
When 'seconds' is not passed in, convert the current time instead.r]  h]r^  (}r_  (h}r`  (hX   secondsra  j�  j�  u�rb  hX�   (tm_year,tm_mon,tm_mday,tm_hour,tm_min,
                          tm_sec,tm_wday,tm_yday,tm_isdst)

Convert seconds since the Epoch to a time tuple expressing local time.
When 'seconds' is not passed in, convert the current time instead.rc  h]rd  j�  au}re  (h(]rf  h�ah-)u}rg  (h(]rh  h�ah-}ri  (h/]rj  h,ah3X   secondsrk  u�rl  ueuuj�  }rm  (hhAh	}rn  (hC]ro  (j�  hKehL]rp  hKahX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    rq  X	   is_hiddenrr  �h}rs  (X   module_reprrt  }ru  (hhh	}rv  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        rw  h]rx  }ry  (h}rz  (hhh�h�u}r{  (hh�h�h�u�r|  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r}  uauuh�}r~  (hhXh	}r  (hX   default object formatterr�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   default object formatterr�  uauuh�}r�  (hhXh	}r�  (hX   Return self<value.r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return self<value.r�  uauuX   exec_moduler�  }r�  (hhh	}r�  (hX   Exec a built-in moduler�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Exec a built-in moduler�  uauuj7  }r�  (hhXh	}r�  (hX   Return str(self).r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return str(self).r�  uauuX   find_moduler�  }r�  (hhh	}r�  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  uauuX   __dict__r�  }r�  (hhqh	}r�  hA]r�  hX   mappingproxyr�  �r�  asuj�  }r�  (hhXh	}r�  (hX   Return hash(self).r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return hash(self).r�  uauuX	   find_specr�  }r�  (hhqh	}r�  hA]r�  hX   methodr�  �r�  asuX
   get_sourcer�  }r�  (hhh	}r�  (hX8   Return None as built-in modules do not have source code.r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX8   Return None as built-in modules do not have source code.r�  uauuX   __weakref__r�  }r�  (hhRh	}r�  (hX2   list of weak references to the object (if defined)r�  hA]r�  hKauujj  }r�  (hjl  h	]r�  jo  auX
   is_packager�  }r�  (hhh	}r�  (hX4   Return False as built-in modules are never packages.r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX4   Return False as built-in modules are never packages.r�  uauuX   get_coder�  }r�  (hhh	}r�  (hX9   Return None as built-in modules do not have code objects.r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX9   Return None as built-in modules do not have code objects.r�  uauuX   create_moduler�  }r�  (hhh	}r�  (hX   Create a built-in moduler�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Create a built-in moduler�  uauuhV}r�  (hhXh	}r�  (hX6   __sizeof__() -> int
size of object in memory, in bytesr�  h]r�  }r�  (h}r�  (hA]r�  h`ahhau�r�  hX"   size of object in memory, in bytesr�  h]r�  hfauauuh�}r�  (hhXh	}r�  (hX   helper for pickler�  h]r�  }r�  (h}r   (hhh�h�u}r  (hh�h�h�u�r  hX   helper for pickler  uauuj"  }r  (hhXh	}r  (hX   Implement delattr(self, name).r  h]r  }r  (h}r	  (hhh�h�u}r
  (hh�h�h�u�r  hX   Implement delattr(self, name).r  uauujG  }r  (hhXh	}r  (hX   Return self==value.r  h]r  }r  (h}r  (hhh�h�u}r  (hh�h�h�u�r  hX   Return self==value.r  uauuj�  }r  (hhqh	}r  hA]r  j�  asuj�  }r  (hhXh	}r  (hX   Return self!=value.r  h]r  }r  (h}r  (hhh�h�u}r  (hh�h�h�u�r   hX   Return self!=value.r!  uauuj�  }r"  (hhXh	}r#  (hX.   __dir__() -> list
default dir() implementationr$  h]r%  }r&  (h}r'  (hA]r(  h`ahhau�r)  hX   default dir() implementationr*  h]r+  j�  auauuj�  }r,  (hhXh	}r-  (hX%   Implement setattr(self, name, value).r.  h]r/  }r0  (h}r1  (hhh�h�u}r2  (hh�h�h�u�r3  hX%   Implement setattr(self, name, value).r4  uauuj�  }r5  (hhXh	}r6  (hX   Return repr(self).r7  h]r8  }r9  (h}r:  (hhh�h�u}r;  (hh�h�h�u�r<  hX   Return repr(self).r=  uauuX   load_moduler>  }r?  (hhh	}r@  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    rA  h]rB  }rC  (h}rD  (hhh�h�u}rE  (hh�h�h�u�rF  hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    rG  uauuX
   __module__rH  }rI  (hhqh	}rJ  hA]rK  j�  asuj*  }rL  (hhh	}rM  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
rN  h]rO  }rP  (h}rQ  (hhh�h�u}rR  (hh�h�h�u�rS  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
rT  uauuj8  }rU  (hhXh	}rV  (hX   Return self>=value.rW  h]rX  }rY  (h}rZ  (hhh�h�u}r[  (hh�h�h�u�r\  hX   Return self>=value.r]  uauujp  }r^  (hhXh	}r_  (hX>   Initialize self.  See help(type(self)) for accurate signature.r`  h]ra  }rb  (h}rc  (hhh�h�u}rd  (hh�h�h�u�re  hX>   Initialize self.  See help(type(self)) for accurate signature.rf  uauuj�  }rg  (hhXh	}rh  (hX   helper for pickleri  h]rj  }rk  (h}rl  (hhh�h�u}rm  (hh�h�h�u�rn  hX   helper for picklero  uauuj  }rp  (hhh	}rq  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rr  h]rs  }rt  (h}ru  (hhh�h�u}rv  (hh�h�h�u�rw  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rx  uauuj  }ry  (hhXh	}rz  (hX   Return self<=value.r{  h]r|  }r}  (h}r~  (hhh�h�u}r  (hh�h�h�u�r�  hX   Return self<=value.r�  uauuj,  }r�  (hhXh	}r�  (hX   Return self>value.r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hX   Return self>value.r�  uauujK  }r�  (hhh	}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h]r�  }r�  (h}r�  (hhh�h�u}r�  (hh�h�h�u�r�  hXG   Create and return a new object.  See help(type) for accurate signature.r�  uauuuuuuu.