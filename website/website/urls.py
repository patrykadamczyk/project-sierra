from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    #url(r'^$', app.views.home, name='home'),
    # Blog
    url(r'^', include('blog.urls')),
    #Wylogowywanie
    url(r'^logout$', django.contrib.auth.views.logout, { 'next_page': '/', }, name='logout'),
    #Panel Administracyjny Django
    url(r'^admin/django/', include(admin.site.urls)),
]
