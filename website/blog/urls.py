from django.conf.urls import include, url
from . import views

app_name = 'blog'

urlpatterns = [
    # Main Page
    #url(r'^$', views.index, name="index"),
    # Registration
    #url(r'^register/$', views.UserFormView.as_view(), name='register'),
    # Login
    #url(r'^login/$', views.LoginFormView.as_view(), name='login'),
    # Logout
    #url(r'^logout/$', views.logout_view, name='logout'),
]